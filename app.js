'use strict';

/* Create student and compile full name */
let student = {};

student.name = prompt('Enter student name: ');
student.lastName = prompt('Enter student last name');

/* Create tabel, iterate asking for subject and grade*/
let studentTabel = {};

while (student) {
    studentTabel.subject = prompt('Enter subject name: ');
    studentTabel.grade = +prompt(`Enter ${studentTabel.subject} grade: `);

    if (studentTabel.subject === '' || studentTabel.subject === null)
    {break}
    Object.defineProperty(studentTabel, `${studentTabel.subject}`, {
        value: studentTabel.grade,
    });
}

/* Output all arrays into the tabel property of student*/
Object.defineProperty(student, 'tabel', {
    value: studentTabel,
    enumerable: true,
});

/* Grade calculations */
let allGrades = [];

let failGrades = () => {
    let failGrades;
    for (let key in student.tabel) {
        if (student.tabel.key < 4) {
            failGrades = 'failed'
        } else {
            allGrades.push(student.tabel.key);
        }
    }
    if (failGrades !== 'failed') {
        alert(`Студент переведен на следующий курс`);
    }
}
failGrades();

let averageGrade = () => {
    let total = 0;
    let counter = 0;
    for (let key in studentTabel) {
        total += +studentTabel.key
        counter++
    }
    if (total / counter > 7) {
        Object.defineProperty(student, 'stipendia', {
            value: true,
            });
        alert( `Студенту назначена стипендия`);
    } else {
        Object.defineProperty(student, 'stipenia', {
            value: false,
        });
    }
}
averageGrade();

console.log(student);
